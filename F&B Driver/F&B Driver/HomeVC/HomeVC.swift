//
//  HomeVC.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 25/08/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire

class HomeVC: UIViewController {

    @IBOutlet weak var viewGoogleMap: GMSMapView!
    @IBOutlet weak var viewUser: UIView!
  
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.bringSubview(toFront: viewUser)
        
        // Add Tap gesture to viewUser
        let tapUser = UITapGestureRecognizer.init(target: self, action: #selector(userTapped(tapped:)))
        viewUser.addGestureRecognizer(tapUser)
        
        
    }
    

    // MARK:- Tap Gesture Function
    func userTapped(tapped:UITapGestureRecognizer){
        self.navigationController?.pushViewController(MyAccountVC(), animated: true)
    }
}
