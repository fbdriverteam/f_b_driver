//
//  UIColor+HashCode.swift
//  projectlib
//
//  Created by Rakesh Kumar on 13/06/16.
//  Copyright © 2016 Baltech. All rights reserved.
//

/*
 How to use example
 
 var color = UIColor(red: 0xFF, green: 0xFF, blue: 0xFF)
 var color = UIColor(netHex:0xFFFFFF)
 
 */
import ObjectiveC
import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, alphaVal: CGFloat) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alphaVal)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff, alphaVal: 1.0)
    }
    
    convenience init(netHex:Int, alphaValue: CGFloat) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff, alphaVal: alphaValue)
    }
}
