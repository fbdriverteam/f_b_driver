//
//  Config.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 23/08/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

struct AppURL {
    static let BASE_URL = "http://192.168.1.89/projects/fnb/web_services/"
}

struct Constants {
    static let defaults = UserDefaults.standard
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
}

//---------------------------------------
// NSUserDefault Constants
//---------------------------------------
struct NSUserDefaultConstants{
    static let KEY_USER_DETAILS_NSDEFAULT = "CurrentLoginUserDetails"
}

//---------------------------------------
// Device
//---------------------------------------
struct DeviceInfo {
    static let DEVICE_IDENTIFIER:String = UIDevice.current.identifierForVendor!.uuidString
    static let DEVICE_OS_VERSION:String = UIDevice.current.systemVersion
    static let DEVICE_PHONE_MODEL:String = UIDevice.current.model
}


var objInfo = UserInfo.init(ID: "", auth: "", name: "", email: "", mobile: "")
// API Headers

enum Header:String {
    case Login = "vendor_login"
}

enum CustomCell:String{
    case SideCell = "sideCell"
    case OrderDetailCell = "OrderDetailCell"
}


let global = Global()
let webCall = Webservices()
class Config: NSObject {
    
    

}
