//
//  AppDelegate.swift
//  F&B Driver
//
//  Created by Mohindra Bhati's mac on 22/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navController: UINavigationController?
    var DeviceToken : String = ""


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        GMSServices.provideAPIKey("AIzaSyBye0P47xar9-ULsehyaPjARfLLxSPMkC8")
        registerForPushNotifications(application: application)
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: true)
        global.setRootViewController()
        return true
    }

    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings)
    {
        if notificationSettings.types != .none
        {
            application.registerForRemoteNotifications()
            
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        var tokenString : String = ""
        for i in 0..<deviceToken.count{
            tokenString += String.init(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        DeviceToken = tokenString.uppercased()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        print("Failed to register:",error)
    }
    
    func registerForPushNotifications(application: UIApplication){
        let notificationSettings = UIUserNotificationSettings.init(types: [.alert, .badge , .sound], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
    }

}

