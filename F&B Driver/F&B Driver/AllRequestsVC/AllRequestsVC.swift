//
//  AllRequestsVC.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 18/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class AllRequestsVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var constFiltersBottomViewLeading: NSLayoutConstraint!
    
    @IBOutlet weak var scrlOrderList: UIScrollView!
    @IBOutlet weak var tblCanceledOrder: UITableView!
    @IBOutlet weak var tblDeliveredOrders: UITableView!
    @IBOutlet weak var tblAllOrders: UITableView!
    @IBOutlet weak var tblInTransitOrders: UITableView!
    

    override func viewDidLoad()
    {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btn_Back_Tapped(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_Filter_Tapped(_ sender: UIButton)
    {
        sender.setTitleColor(UIColor.init(netHex: 0x27C59A), for: .selected)
        switch sender.tag {
        case 101:
            var frame = scrlOrderList.frame
            frame.origin.x = frame.size.width * 0
            frame.origin.y = 0
            scrlOrderList.scrollRectToVisible(frame, animated: true)
            self.constFiltersBottomViewLeading.constant = 0
           
            UIView.animate(withDuration: 0.5, animations:
                {
                self.view.layoutIfNeeded()
            })
            break
        case 102:
            var frame = scrlOrderList.frame
            frame.origin.x = frame.size.width * 1
            frame.origin.y = 0
            scrlOrderList.scrollRectToVisible(frame, animated: true)
            self.constFiltersBottomViewLeading.constant = sender.layer.frame.width * 1
            
            UIView.animate(withDuration: 0.5, animations:
                {
                 self.view.layoutIfNeeded()
            })
            break
        case 103:
            var frame = scrlOrderList.frame
            frame.origin.x = frame.size.width * 2
            frame.origin.y = 0
            scrlOrderList.scrollRectToVisible(frame, animated: true)
            self.constFiltersBottomViewLeading.constant = sender.layer.frame.width * 2
            
            UIView.animate(withDuration: 0.5, animations:
                {
                self.view.layoutIfNeeded()
            })
            break
        case 104:
            var frame = scrlOrderList.frame
            frame.origin.x = frame.size.width * 3
            frame.origin.y = 0
            scrlOrderList.scrollRectToVisible(frame, animated: true)
            self.constFiltersBottomViewLeading.constant = sender.layer.frame.width * 3
            
            UIView.animate(withDuration: 0.5, animations:
                {
                self.view.layoutIfNeeded()
            })
            break
        default:
            break
        }
    }
    
    
    //MARK:- UITableViewDelegate & DataSource
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch tableView {
        case tblAllOrders:
            return true
        case tblInTransitOrders:
            return true
        case tblDeliveredOrders:
            return true
        case tblCanceledOrder:
            return true
        default:
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let viewDetails : UITableViewRowAction = UITableViewRowAction.init(style: .normal, title: "View\nDetails", handler: {index in
           self.navigationController?.pushViewController(OrderDetailsVC(), animated: true)
        })
        viewDetails.backgroundColor = UIColor.init(netHex: 0x27C59A)
        return [viewDetails]
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        tableView.register(UINib.init(nibName: "OrdersCell", bundle: nil), forCellReuseIdentifier: "OrdersCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath) as! OrdersCell
        
        if indexPath.row == 0 || indexPath.row == 3 || indexPath.row == 6
        {
            cell.lblPaymentMode.text = "payment Mode: CC"
            cell.lblTotalAmount.text = "$10.53"
            cell.lblStatus.text = "In-transit"
            cell.lblStatus.textColor = UIColor.init(netHex: 0xFFBA30)
        }
        else if indexPath.row == 1 || indexPath.row == 4 || indexPath.row == 7
        {
            cell.lblPaymentMode.text = "payment Mode: CASH"
            cell.lblTotalAmount.text = "$15.50"
            cell.lblStatus.text = "Delivered"
            cell.lblStatus.textColor = UIColor.init(netHex: 0x27C59A)
        }
        
        else if indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 8
        {
            cell.lblPaymentMode.text = "payment Mode: PREPAID"
            cell.lblTotalAmount.text = "$9.97"
            cell.lblStatus.text = "Canceled"
            cell.lblStatus.textColor = UIColor.init(netHex: 0xE6220C)
        }
        
        switch tableView
        {
        case tblAllOrders:
            break
        case tblInTransitOrders:
            break
        case tblDeliveredOrders:
            break
        case tblCanceledOrder:
            break
            
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10
    }
   
}
