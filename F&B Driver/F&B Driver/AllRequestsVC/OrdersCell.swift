//
//  OrdersCell.swift
//  F&B Services
//
//  Created by Mohindra Bhati's mac on 19/09/17.
//  Copyright © 2017 Octal. All rights reserved.
//

import UIKit

class OrdersCell: UITableViewCell {
    
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

    }
    
}
